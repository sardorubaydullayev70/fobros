import axios from 'axios';

// @ts-ignore
const getAllStates = async (searchQuery,page) => {
    try {
        const response = await axios.get('/api/v1/locations/', {
            params: {
                searchText: searchQuery,
                page: page
            }
        });
        return response.data;
    } catch (error) {
        console.error(error);
        return []; // Return an empty array or handle the error case as needed
    }
};

export default getAllStates;
