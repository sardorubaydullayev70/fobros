import {createBrowserRouter} from "react-router-dom";
import Home from "./homepage/HomePage";
import ContactUsPage from "./ContactUsPage";
import Layout from "./Layout";
import React from "react";
import AboutUsPage from "./AboutUsPage";
import CompanyPage from "./CompanyPage";
import BlogPage from "./BlogPage";

const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout/>,
        children: [
            {index: true, element: <Home/>},
            {path: "about-us", element: <AboutUsPage/>},
            {path: "company", element: <CompanyPage/>},
            {path: "blog", element: <BlogPage/>},
            {path: "contact-us", element: <ContactUsPage/>},
        ]
    }
])

export default router;