import React, {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEnvelope, faPhone,} from '@fortawesome/free-solid-svg-icons';
import {Link} from "react-router-dom";
import QuoteForm from "../components/QuoteForm";
import USAMap from "../components/USAMap";

const AboutUsPage = () => {

    const [pickupLocationCode, setPickupLocationCode] = useState('');
    const [deliveryLocationCode, setDeliveryLocationCode] = useState('');



    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    return (
        <>
            <div
                className="site-blocks-cover inner-page-cover overlay"
                style={{backgroundImage: "url(/assets/images/about-us.jpg)"}}
                data-aos="fade"
                data-stellar-background-ratio="0.5"
            >
                <div className="container">
                    <div className="row align-items-center justify-content-center text-center">

                        <div className="col-md-8" data-aos="fade-up" data-aos-delay="200">
                            <h1 className="text-white font-weight-light text-uppercase font-weight-bold">About Us</h1>
                            <p className="breadcrumb-custom"><Link to="/">Home</Link> <span
                                className="mx-2">&gt;</span> <span>About Us</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="site-section">
                <div className="container">

                    <div className="order-md-1" data-aos="fade">
                        <div className="text-left pb-1 border-primary mb-4">
                            <h2 className="text-primary">Our History</h2>
                        </div>
                        <div className={'row justify-content-around'}>
                            <p className={'col-md-6 ml-auto mb-5 order-md-2'}>
                                Fobros Group LLC is a logistics company that provides a wide range of transportation
                                services to customers throughout the United States and internationally. With years
                                of
                                experience in the industry, Fobros Group LLC has built a reputation for providing
                                safe,
                                reliable, and affordable logistics solutions. The company offers a variety of
                                transportation modes, including car shipping, motorcycle shipping, boat shipping,
                                heavy
                                haul trailer shipping, and international shipping. Fobros Group LLC prides itself on
                                its
                                commitment to exceptional customer service, personalized attention, competitive
                                pricing,
                                and timely delivery. Its team of experienced logistics professionals works closely
                                with
                                customers to create customized shipping plans that meet their specific needs and
                                requirements.
                            </p>
                            <div className="col-md-6 ml-auto mb-5 order-md-2" data-aos="fade">
                                <img

                                    src="/assets/images/our-history.jpg"
                                    alt="Image"
                                    className="float-right rounded"
                                    width={'450px'}
                                />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div
                className="site-section bg-image overlay"
                style={{backgroundImage: 'url(/assets/images/our-history.jpg)'}}
            >
                <div className="container">
                    <div className="row justify-content-center mb-5">
                        <div className="col-md-7 text-center border-primary">
                            <h2 className="font-weight-light text-primary" data-aos="fade">
                                How It Works
                            </h2>
                        </div>
                    </div>
                    <div className="row">
                        <div
                            className="col-md-6 col-lg-4 mb-5 mb-lg-0"
                            data-aos="fade"
                            data-aos-delay={100}
                        >
                            <div className="how-it-work-item">
                                <span className="number">1</span>
                                <div className="how-it-work-body">
                                    <h2>Book your vehicle transportation online or by phone.</h2>
                                    <p className="mb-5">
                                        It's simple to send a car using Fobros Group LLC!
                                        Using our online car shipping quotation tool, you just submit some necessary
                                        details of your shipment such as pickup and destination zip codes, trailer type,
                                        vehicle details and shipping dates.

                                        Then one of our available agents will manually calculate the best competitive
                                        quotes and get back to you shortly via email and phone call. Then you can go
                                        ahead and compare our prices and services (we know you want to!) — we’re sure
                                        you’ll find our prices very competitive.

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div
                            className="col-md-6 col-lg-4 mb-5 mb-lg-0"
                            data-aos="fade"
                            data-aos-delay={200}
                        >
                            <div className="how-it-work-item">
                                <span className="number">2</span>
                                <div className="how-it-work-body">
                                    <h2>We pick up your vehicle</h2>
                                    <p className="mb-5">
                                        Fobros Group LLC’s experts will work with you to set up a pick-up time and date
                                        that works best for you. As a direct customer of our company, we do our best to
                                        work around your schedule and assign only trusted truckers for your shipment.

                                        When your trucker arrives, both of you will inspect your car together and sign a
                                        Bill of Lading. Then the assigned trucker will take care of the rest.

                                        While your car is enjoying its journey, you can check in with your trucker
                                        directly as often as you wish to get updates.

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div
                            className="col-md-6 col-lg-4 mb-5 mb-lg-0"
                            data-aos="fade"
                            data-aos-delay={300}
                        >
                            <div className="how-it-work-item">
                                <span className="number">3</span>
                                <div className="how-it-work-body">
                                    <h2>Receive your vehicle</h2>
                                    <p className="mb-5">
                                        You will receive a short update from the driver some time before the drop off
                                        which should leave you with enough preparation time. Then your assigned trucker
                                        will deliver your vehicle to your doorsteps (as we offer door-to-door service)
                                        or any other drop off location of your convenience.

                                        After a careful inspection to make sure everything looks good, you’ll simply
                                        sign the Bill of landing to accept your delivery. That’s it!

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="site-section border-bottom">
                <div className="container">
                    <div className="row justify-content-center mb-5">
                        <div className="col-md-7 text-center border-primary">
                            <h2 className="font-weight-light text-primary" data-aos="fade">
                                Our Team
                            </h2>
                        </div>
                    </div>
                    <div className="row">
                        <div
                            className="col-md-3 col-lg-3 mb-5 mb-lg-0 text-center py-4"
                            data-aos="fade"
                            data-aos-delay={100}
                            style={{
                                border: '1px solid orange',
                                borderRadius: '5px'
                            }}
                        >
                            <div className="person">
                                <img
                                    src="/assets/images/james-smith.jpg"
                                    alt="Image"
                                    className="img-fluid  mb-5"
                                    style={{
                                        objectFit: 'cover',
                                        width: '150px',
                                        height: '150px',
                                        borderRadius: '50%',
                                    }}

                                />
                                <h3 style={{color: 'orange'}} className={'font-weight-bold'}>James Smith</h3>
                                <p className="position">Co-founder and CEO</p>

                                <ul className="ul-social-circle d-flex justify-content-center">
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`tel:111111`} className={'m-0 ml-3'}> <FontAwesomeIcon icon={faPhone}
                                                                                                        size="2x"
                                                                                                        style={{color: 'orange'}}/>
                                        </a>
                                    </li>
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`mailto:example@mail.com`} className={'m-0 ml-3'}>
                                            <FontAwesomeIcon
                                                icon={faEnvelope} size="2x" style={{color: 'orange'}}/>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div
                            className="col-md-3 col-lg-3 mb-5 mb-lg-0 text-center py-4"
                            data-aos="fade"
                            data-aos-delay={100}
                            style={{
                                border: '1px solid orange',
                                borderRadius: '5px'
                            }}
                        >
                            <div className="person">
                                <img
                                    src="/assets/images/Anthony-Johnson.jpg"
                                    alt="Image"
                                    className="img-fluid  mb-5"
                                    style={{
                                        objectFit: 'cover',
                                        width: '150px',
                                        height: '150px',
                                        borderRadius: '50%',
                                    }}

                                />
                                <h3 style={{color: 'orange'}} className={'font-weight-bold'}>Anthony Johnson</h3>
                                <p className="position">Manager, Accountant, Auto Transport Export</p>

                                <ul className="ul-social-circle d-flex justify-content-center">
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`tel:111111`} className={'m-0 ml-3'}> <FontAwesomeIcon icon={faPhone}
                                                                                                        size="2x"
                                                                                                        style={{color: 'orange'}}/>
                                        </a>
                                    </li>
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`mailto:example@mail.com`} className={'m-0 ml-3'}>
                                            <FontAwesomeIcon
                                                icon={faEnvelope} size="2x" style={{color: 'orange'}}/>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div
                            className="col-md-3 col-lg-3 mb-5 mb-lg-0 text-center py-4"
                            data-aos="fade"
                            data-aos-delay={100}
                            style={{
                                border: '1px solid orange',
                                borderRadius: '5px'
                            }}
                        >
                            <div className="person">
                                <img
                                    src="/assets/images/Tony-Davis.jpg"
                                    alt="Image"
                                    className="img-fluid  mb-5"
                                    style={{
                                        objectFit: 'cover',
                                        width: '150px',
                                        height: '150px',
                                        borderRadius: '50%',
                                    }}

                                />
                                <h3 style={{color: 'orange'}} className={'font-weight-bold'}>Tony Davis</h3>
                                <p className="position">Team Lead / Auto Transport Export</p>

                                <ul className="ul-social-circle d-flex justify-content-center">
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`tel:111111`} className={'m-0 ml-3'}> <FontAwesomeIcon icon={faPhone}
                                                                                                        size="2x"
                                                                                                        style={{color: 'orange'}}/>
                                        </a>
                                    </li>
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`mailto:example@mail.com`} className={'m-0 ml-3'}>
                                            <FontAwesomeIcon
                                                icon={faEnvelope} size="2x" style={{color: 'orange'}}/>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div
                            className="col-md-3 col-lg-3 mb-5 mb-lg-0 text-center py-4"
                            data-aos="fade"
                            data-aos-delay={100}
                            style={{
                                border: '1px solid orange',
                                borderRadius: '5px'
                            }}
                        >
                            <div className="person">
                                <img
                                    src="/assets/images/Alex-Scott.jpg"
                                    alt="Image"
                                    className="img-fluid  mb-5"
                                    style={{
                                        objectFit: 'cover',
                                        width: '150px',
                                        height: '150px',
                                        borderRadius: '50%',
                                    }}

                                />
                                <h3 style={{color: 'orange'}} className={'font-weight-bold'}>David Brown</h3>
                                <p className="position">Chief Marketing Officer</p>

                                <ul className="ul-social-circle d-flex justify-content-center">
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`tel:111111`} className={'m-0 ml-3'}> <FontAwesomeIcon icon={faPhone}
                                                                                                        size="2x"
                                                                                                        style={{color: 'orange'}}/>
                                        </a>
                                    </li>
                                    <li className={'d-flex align-items-center'}>
                                        <a href={`mailto:example@mail.com`} className={'m-0 ml-3'}>
                                            <FontAwesomeIcon
                                                icon={faEnvelope} size="2x" style={{color: 'orange'}}/>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container my-5 pt-md-0 pt-5">
                <div className="row justify-content-center mb-5">
                    <div className="col-md-7 text-center border-primary">
                        <h2 className="font-weight-light text-primary" data-aos="fade">
                            Start your Free Quote
                        </h2>
                    </div>
                </div>

                <div className="row align-items-center justify-content-between">
                    <div className="col-md-4">
                        <QuoteForm formId={'2'}
                                   getPickupLocationCode={(val) => setPickupLocationCode(val)}
                                   getDeliveryLocationCode={val => setDeliveryLocationCode(val)}
                        />
                    </div>
                    <div className="col-md-8 text-right" data-aos="fade-up" data-aos-delay={400}>
                        <div>
                            <USAMap
                                selectedPickupLocationCode={pickupLocationCode}
                                selectedDeliveryLocationCode={deliveryLocationCode}
                            />
                        </div>
                    </div>
                </div>
            </div>



        </>
    );
};

export default AboutUsPage;