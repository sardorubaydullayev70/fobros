import {Link, useLocation} from "react-router-dom";
import React, {useEffect} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBullseye, faHeart, faLightbulb, faStar, faUser, faUsers} from '@fortawesome/free-solid-svg-icons';

function CompanyPage() {

    const location = useLocation();


    useEffect(() => {
        const currentHash = location.hash;
        let clickedSection = ''
        switch (currentHash) {
            case '#reviews':
                clickedSection = 'reviews'
                break;
            case '#vision-and-mission':
                clickedSection = 'vision-and-mission'
                break;
            case '#leadership-team':
                clickedSection = 'leadership-team'
                break;
        }
        const el = document.getElementById(clickedSection);
        if (el) {
            el.scrollIntoView({behavior: 'smooth'});
        }
    }, [location]);

    const stars = [];

    for (let i = 0; i < 5; i++) {
        stars.push(<FontAwesomeIcon key={i} icon={faStar} className="rating-color"/>);
    }

    return (
        <>
            {/*HEADER*/}
            <div
                className="site-blocks-cover inner-page-cover overlay"
                style={{backgroundImage: "url(/assets/images/company.jpg)"}}
                data-aos="fade"
                data-stellar-background-ratio="0.5"
            >
                <div className="container">
                    <div className="row align-items-center justify-content-center text-center">

                        <div className="col-md-8" data-aos="fade-up" data-aos-delay="200">
                            <h1 className="text-white font-weight-light text-uppercase font-weight-bold">Company</h1>
                            <p className="breadcrumb-custom"><Link to="/">Home</Link> <span
                                className="mx-2">&gt;</span> <span>Company</span></p>
                        </div>
                    </div>
                </div>
            </div>

            {/*SECTION*/}
            <div id={'reviews'} className={'site-section py-5'}>
                <div className="container pt-md-0 pt-5" data-aos="fade-up" data-aos-delay={400}>
                    <div>
                        <h1 className={'text-orange'}>Fobros Transport Reviews</h1>
                        <p className={''}>We want our customers to be fully satisfied and nothing makes us happier than
                            getting
                            positive car transport reviews.</p>
                    </div>
                    <div className={' bg-white py-4 px-5 w-75 shadow mx-auto mt-5 '}>
                        <p className={'d-block h-25'}>Five star auto transport reviews</p>
                        <div className="row my-5 justify-content-between">
                            <div className="col-md-6 row">
                                <div className="col-md-6 text-center my-4">
                                    <FontAwesomeIcon
                                        icon={faUsers}
                                        size={'2x'}
                                        color={'orange'}
                                    />
                                    <h6>500+</h6>
                                    Google reviews
                                </div>
                                <div className="col-md-6 text-center my-4">
                                    <FontAwesomeIcon
                                        icon={faUsers}
                                        size={'2x'}
                                        color={'orange'}
                                    />
                                    <h6>120+</h6>
                                    Trustpilot reviews
                                </div>
                                <div className="col-md-6 text-center my-4">
                                    <FontAwesomeIcon
                                        icon={faUsers}
                                        size={'2x'}
                                        color={'orange'}
                                    />
                                    <h6>200+</h6>
                                    Auto Transport Reviews
                                </div>
                                <div className="col-md-6 text-center my-4">
                                    <FontAwesomeIcon
                                        icon={faUsers}
                                        size={'2x'}
                                        color={'orange'}
                                    />
                                    <h6>50+</h6>
                                    Mymovingreviews
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="text-center d-flex flex-column">
                                    <h1 className={'total-reviews-rate'}>5.0</h1>
                                    <div className="ratings">{stars}</div>
                                    <p>Our reputation from auto transporter reviews</p></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div id={'vision-and-mission'} className="site-section pt-5 bg-light">
                <div className="container">
                    <div className="row justify-content-center mb-5">
                        <div className="col-md-7 text-center border-primary">
                            <h2 className="font-weight-light text-primary" data-aos="fade">
                                Vision and mission
                            </h2>
                        </div>
                    </div>

                    <div className="row align-items-stretch">
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="unit-4 d-flex">
                                <div className="unit-4-icon mr-4">
                                    <FontAwesomeIcon icon={faLightbulb} size={'2x'} color={'orange'}/>
                                </div>
                                <div>
                                    <h3>Our vision</h3>
                                    <p>
                                        At Fobros Group LLC, our mission is to make transportation of the vehicles as
                                        easy, safe, and affordable as possible nationwide and build trust and
                                        communicate well with our customers throughout all the steps.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="unit-4 d-flex">
                                <div className="unit-4-icon mr-4">
                                    <FontAwesomeIcon icon={faBullseye} size={'2x'} color={'orange'}/>

                                </div>
                                <div>
                                    <h3>Our goal</h3>
                                    <p>Our ultimate goal is to become all-in-one Auto Transportation company to service
                                        vehicle shipment needs of any kind and size both nationally and globally.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="unit-4 d-flex">
                                <div className="unit-4-icon mr-4">
                                    <FontAwesomeIcon icon={faHeart} size={'2x'} color={'orange'}/>
                                </div>
                                <div>
                                    <h3>Our values</h3>
                                    <p>
                                        Here at Fobros Trucking, we deeply believe in essence of customer service,
                                        innovations, credibility and their role in our business growth. These principles
                                        form a very foundation of our business strategy and we strive to follow them by
                                        all means.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id={'leadership-team'} className="site-section pt-5">
                <div className="container">
                    <div className="row justify-content-center mb-5">
                        <div className="col-md-7 text-center border-primary">
                            <h2 className="font-weight-light text-primary" data-aos="fade">
                                Leadership Team
                            </h2>
                        </div>
                    </div>

                    <div className="mb-4 mb-lg-4">
                        <div className="unit-4 d-flex">
                            <div className="unit-4-icon mr-4">
                                <FontAwesomeIcon icon={faUser} size={'2x'} color={'orange'}/>
                            </div>
                            <div>
                                <h3>James Smith </h3>
                                <p>
                                    James Smith is the Managing Director of Fobros Group LLC, responsible for overseeing
                                    the day-to-day operations of the company. With over 5 years of experience in the
                                    auto transport industry, he brings a wealth of knowledge and expertise to his role.
                                    Before joining Fobros Group LLC, James held various leadership positions in
                                    logistics and transportation companies, where he gained extensive experience in
                                    fleet management, operations, and customer service. He is committed to providing
                                    excellent service to all customers and ensuring that every shipment is delivered
                                    safely and on time.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="mb-4 mb-lg-4">
                        <div className="unit-4 d-flex">
                            <div className="unit-4-icon mr-4">
                                <FontAwesomeIcon icon={faUser} size={'2x'} color={'orange'}/>

                            </div>
                            <div>
                                <h3>Anthony Johnson</h3>
                                <p>Anthony Johnson is an experienced HR professional who currently serves as the HR
                                    manager at Fobros Group LLC. With over a decade of experience in human resources,
                                    Anthony brings a wealth of knowledge and expertise to his role. Anthony is known for
                                    his strong communication and leadership skills, and his ability to work
                                    collaboratively with other departments to achieve organizational goals. He is
                                    committed to fostering a positive and inclusive work environment where all employees
                                    feel valued and supported.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id={'call-us'} className="bg-warning">
                <div className="d-flex justify-content-between">
                    <div className="call-us-contact">
                        <div className="unit-4 text-white">
                            <h1>Call us 24/7 at <a className={'text-white'} href="tel:+3318085858">(331) 808 58 58</a></h1>
                            <p>
                                <a href="/#" className="btn btn-light text-orange  py-3 px-5">
                                    Get a Quote!
                                </a>
                            </p>
                        </div>
                    </div>
                    <div className={'d-flex justify-content-end'}>
                        <img className={'call-us-img'} src="/assets/images/call-us.png" alt="call-us"/>
                    </div>

                </div>
            </div>

        </>
    );
}

export default CompanyPage;
