import {Link, useLocation} from "react-router-dom";
import React, {useEffect} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar, faUsers, faLightbulb, faBullseye, faHeart, faUser} from '@fortawesome/free-solid-svg-icons';

function BlogPage() {

    const location = useLocation();


    useEffect(() => {
        const currentHash = location.hash;
        let clickedSection = ''
        switch (currentHash) {
            case '#reviews':
                clickedSection = 'reviews'
                break;
            case '#vision-and-mission':
                clickedSection = 'vision-and-mission'
                break;
            case '#leadership-team':
                clickedSection = 'leadership-team'
                break;
        }
        const el = document.getElementById(clickedSection);
        if (el) {
            el.scrollIntoView({behavior: 'smooth'});
        }
    }, [location]);

    const stars = [];

    for (let i = 0; i < 5; i++) {
        stars.push(<FontAwesomeIcon key={i} icon={faStar} className="rating-color"/>);
    }

    return (
        <>
            <div
                className="site-blocks-cover inner-page-cover overlay"
                style={{backgroundImage: "url(/assets/images/blog_2.jpg)"}}
                data-aos="fade"
                data-stellar-background-ratio="0.5"
            >
                <div className="container">
                    <div className="row align-items-center justify-content-center text-center">
                        <div className="col-md-8" data-aos="fade-up" data-aos-delay={400}>
                            <h1 className="text-white font-weight-light text-uppercase font-weight-bold">
                                Our Blog
                            </h1>
                            <p className="breadcrumb-custom">
                                <a href="index.html">Home</a> <span className="mx-2">&gt;</span>{" "}
                                <span>Blog</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="site-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_1.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_2.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_1.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_2.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_1.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4">
                            <div className="h-entry">
                                <img src="/assets/images/blog_2.jpg" alt="Image" className="img-fluid"/>
                                <h2 className="font-size-regular">
                                    <a href="#">Warehousing Your Packages</a>
                                </h2>
                                <div className="meta mb-4">
                                    Theresa Winston <span className="mx-2">•</span> Jan 18, 2019
                                    <span className="mx-2">•</span> <a href="#">News</a>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus
                                    eligendi nobis ea maiores sapiente veritatis reprehenderit
                                    suscipit quaerat rerum voluptatibus a eius.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container text-center pb-5">
                <div className="row">
                    <div className="col-12">
                        <p className="custom-pagination">
                            <span>1</span>
                            <a href="#">2</a>
                            <a href="#">3</a>
                        </p>
                    </div>
                </div>
            </div>
        </>
    );
}

export default BlogPage;
