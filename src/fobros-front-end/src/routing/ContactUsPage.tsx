import {Link} from "react-router-dom";
import React from "react";
import QuoteForm from "../components/QuoteForm";

function ContactUsPage() {
    return (
        <>
            {/*HEADER*/}
            <div
                className="site-blocks-cover inner-page-cover overlay"
                style={{backgroundImage: "url(/assets/images/contact-page.jpg)"}}
                data-aos="fade"
                data-stellar-background-ratio="0.5"
            >
                <div className="container">
                    <div className="row align-items-center justify-content-center text-center">

                        <div className="col-md-8" data-aos="fade-up" data-aos-delay="200">
                            <h1 className="text-white font-weight-light text-uppercase font-weight-bold">Contact Us</h1>
                            <p className="breadcrumb-custom"><Link to="/">Home</Link> <span
                                className="mx-2">&gt;</span> <span>Contact</span></p>
                        </div>
                    </div>
                </div>
            </div>

            {/*SECTION*/}
            <div className={'py-5'}>
                <div className="container pt-md-0 pt-5">
                    <div className="row align-items-center justify-content-center">
                        <div className="col-md-8" data-aos="fade-up" data-aos-delay={400}>
                            <h1 className={`text-orange font-weight-light mb-5 text-uppercase font-weight-bold`}>Let’s
                                get in touch and ship your vehicle safely and securely</h1>
                            <p>
                                <a href="/#" className="btn btn-primary py-3 px-5 text-white">
                                    Get a Quote!
                                </a>
                            </p>
                        </div>
                        <div className="col-md-4">
                            <QuoteForm
                                formId={'3'}
                                quoteFormTitle={'Start your Free Quote'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ContactUsPage;