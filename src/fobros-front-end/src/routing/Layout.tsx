import React from 'react';
import NavBar from "../components/NavBar";
import {Outlet} from "react-router-dom";
import Footer from "../components/Footer";
import ScrollToTop from "../components/ScrollToTop";

const Layout = () => {

    return (
        <>
            <ScrollToTop/>
            <div className="marquee">
              <span>
                Ship your vehicle safely and securely with Fobros Group LLC. Get a <a href="#/"
                                                                                      style={{color: "#F89D13FF"}}>Quote</a> now and experience hassle-free auto transport services
              </span>
            </div>
            <NavBar/>
            <div id="main" className={'site-wrap'}>
                <Outlet/>
            </div>
            <Footer/>
        </>
    );
};

export default Layout;