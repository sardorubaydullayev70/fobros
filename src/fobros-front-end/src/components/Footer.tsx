import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAddressCard, faCalendar, faEnvelope, faFax, faPhone, faStar} from "@fortawesome/free-solid-svg-icons";
import {NavLink} from "react-router-dom";

const Footer = () => {

    const fiveStar = () => {
        return (<>
            <FontAwesomeIcon icon={faStar} className="rating-color"/>
            <FontAwesomeIcon icon={faStar} className="rating-color"/>
            <FontAwesomeIcon icon={faStar} className="rating-color"/>
            <FontAwesomeIcon icon={faStar} className="rating-color"/>
            <FontAwesomeIcon icon={faStar} className="rating-color"/>
        </>);
    }
    return (
        <>
            <footer className="site-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            {/*COMPANY LOGO*/}
                            <NavLink to={'/'} className="text-white mb-4">
                                <img width={"170px"} src="/assets/images/logo.png" alt=""/>
                            </NavLink>
                            <p className={'mt-3'}>At Fobros Group LLC, we pride ourselves on offering competitive and
                                transparent
                                pricing for our auto transport services. Our experienced team provides
                                comprehensive protection for your vehicle during transit, giving you peace of
                                mind. We're fully licensed and insured, with MC #1459927 and USDOT #3935450.
                                Contact us today to learn more and get a quote.</p>
                        </div>
                        <div className="col-md-3">
                            <h2 className="footer-heading text-uppercase mb-4">Contact Us</h2>
                            <ul className="list-unstyled">
                                <li className={'d-flex align-items-center'}>
                                    <FontAwesomeIcon icon={faAddressCard} size="1x" style={{color: 'white'}}/>
                                    <p className={'m-0 ml-3'}>3343 Silverton Dr. Katy, TX 77494, United States </p>
                                </li>
                                <li className={'d-flex align-items-center'}>
                                    <FontAwesomeIcon icon={faPhone} size="1x" style={{color: 'white'}}/>
                                    <p className={'m-0 ml-3'}>Phone: <a href="tel:+3318085858">(331)-808-5858</a></p>
                                </li>
                                <li className={'d-flex align-items-center'}>
                                    <FontAwesomeIcon icon={faFax} size="1x" style={{color: 'white'}}/>
                                    <p className={'m-0 ml-3'}>Fax: <a href="fax:+208.593.3010">(208)-593-3010</a></p>
                                </li>
                                <li className={'d-flex align-items-center'}>
                                    <FontAwesomeIcon icon={faEnvelope} size="1x" style={{color: 'white'}}/>
                                    <p className={'m-0 ml-3'}>Email: <a
                                        href="mailto:fobrosgroupllc@gmail.com">fobrosgroupllc@gmail.com</a></p>
                                </li>
                                <li className={'d-flex align-items-center'}>
                                    <FontAwesomeIcon icon={faCalendar} size="1x" style={{color: 'white'}}/>
                                    <p className={'m-0 ml-3'}>Mon-Fri: 8am-7pm CST<br/>Sut-Sun: 9am-5pm</p>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <h2 className="footer-heading mb-4">Reviews</h2>
                            <ul className="list-unstyled">
                                <li>
                                    <p className={'m-0'}>TRANSPORT REVIEWS</p>
                                    {fiveStar()}
                                    <p>(128 Reviews)</p>
                                </li>
                                <li>
                                    <p className={'m-0'}>FACEBOOK</p>
                                    {fiveStar()}
                                    <p>(24 Reviews)</p>
                                </li>
                                <li>
                                    <p className={'m-0'}>GOOGLE</p>
                                    {fiveStar()}
                                    <p>(700 Reviews)</p>
                                </li>
                                <li>
                                    <p className={'m-0'}>TRUSTPILOT</p>
                                    {fiveStar()}
                                    <p>(210 Reviews)</p>
                                </li>

                            </ul>
                        </div>
                        <div className="col-md-3">
                            <h2 className="footer-heading mb-4">Quick Links</h2>
                            <ul className="list-unstyled">
                                <li><NavLink to={'/about-us'}>About Us</NavLink></li>
                                <li><NavLink to={'/company'}>Company</NavLink></li>
                                <li><NavLink to={'/blog'}>Blog</NavLink></li>
                                <li><NavLink to={'/contact-us'}>Contact Us</NavLink></li>
                            </ul>
                        </div>
                    </div>
                    <div className="row pt-5 mt-5 text-center">
                        <div className="col-md-12">
                            <div className="border-top pt-5">
                                <p>Copyright © All rights reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
};

export default Footer;