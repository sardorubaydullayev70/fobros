import React, {useEffect, useState} from 'react';
import {Dropdown, Form, FormControl, InputGroup} from "react-bootstrap";
import citiesJson from "../../public/assets/citiesJson.json";
import getAllStates from "../request";


interface Props {
    getPickupLocationCode?: (val: string) => void;
    getDeliveryLocationCode?: (val: string) => void;
    formId: string;
    quoteFormTitle?: string;
}

const QuoteForm = ({getPickupLocationCode, getDeliveryLocationCode, formId, quoteFormTitle}: Props) => {

    useEffect(() => {
        getAllStates('', 10)
            .then(value => {
                setCities(value.content)

            })
    }, [])

    const [showPickupLocation, setShowPickupLocation] = useState(false);
    const [showDeliveryLocation, setShowDeliveryLocation] = useState(false);
    const [selectedPickupLocation, setSelectedPickupLocation] = useState('');
    const [selectedDeliveryLocation, setSelectedDeliveryLocation] = useState('');
    const [cities, setCities] = useState(citiesJson);
    const [transportType, setTransportType] = useState<string>('none');

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTransportType(e.target.value);
    };
    const handleSelectPickupLocation = (selectedValue: string | null) => {
        if (selectedValue) {
            setSelectedPickupLocation(selectedValue);
            if (getPickupLocationCode) {
                getPickupLocationCode(selectedValue.split(',')[2].trim())
            }
            setShowPickupLocation(false);
        }
    };
    const handleSelectDeliveryLocation = (selectedValue: string | null) => {
        if (selectedValue) {
            setSelectedDeliveryLocation(selectedValue);
            if (getDeliveryLocationCode) {
                getDeliveryLocationCode(selectedValue.split(',')[2].trim())
            }
            setShowDeliveryLocation(false);
        }
    };

    const filteredItemsPickup = cities.filter(
        city =>
            city.name.toLowerCase().includes(selectedPickupLocation.toLowerCase()) ||
            city.city.toLowerCase().includes(selectedPickupLocation.toLowerCase()) ||
            city.state.toLowerCase().includes(selectedPickupLocation.toLowerCase())
    );

    const filteredItemsDelivery = cities.filter(
        city =>
            city.name.toLowerCase().includes(selectedDeliveryLocation.toLowerCase()) ||
            city.city.toLowerCase().includes(selectedDeliveryLocation.toLowerCase()) ||
            city.state.toLowerCase().includes(selectedDeliveryLocation.toLowerCase())
    );

    const handleChangePickupLocation = (value: string) => {
        setShowPickupLocation(true)
        setShowDeliveryLocation(false)
        setSelectedPickupLocation(value)
        getAllStates(value, 1).then(res => {
            setCities(res.content)
        })
    }

    const handleChangeDeliveryLocation = (value: string) => {
        setShowDeliveryLocation(true)
        setShowPickupLocation(false)
        setSelectedDeliveryLocation(value)
        getAllStates(value, 1).then(value => {
            setCities(value.content)
        })
    }

    return (
        <>
            <div className="form-panel">
                <Form>
                    <p style={{color: "#666"}}>{quoteFormTitle}</p>
                    {/* === PICKUP LOCATION === */}
                    <Form.Group className="mb-3">
                        <Form.Label>Pickup location</Form.Label>
                        <InputGroup onClick={() => {
                            setShowPickupLocation(!showPickupLocation)
                            setShowDeliveryLocation(false)
                        }}>
                            <FormControl
                                style={{fontSize: 12}}
                                placeholder="Enter zip code or city"
                                value={selectedPickupLocation}
                                onChange={(e) => handleChangePickupLocation(e.target.value)}
                            />
                            <InputGroup>
                                <Dropdown show={showPickupLocation}
                                          onSelect={handleSelectPickupLocation}>
                                    {/*<Dropdown.Toggle split variant="outline-secondary" id="dropdown-split-basic" />*/}

                                    <Dropdown.Menu className="dropdown-scrollable">
                                        {filteredItemsPickup.map((item, idx) => (
                                            <Dropdown.Item
                                                eventKey={`${item.name}, ${item.city}, ${item.state}`}
                                                key={idx}>
                                                {`${item.name}, ${item.city}, ${item.state}`}
                                            </Dropdown.Item>
                                        ))}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </InputGroup>
                        </InputGroup>
                    </Form.Group>
                    {/* === DELIVERY LOCATION === */}
                    <Form.Group className="mb-3">
                        <Form.Label>Delivery location</Form.Label>
                        <InputGroup onClick={() => {
                            setShowDeliveryLocation(!showDeliveryLocation)
                            setShowPickupLocation(false)
                        }}>
                            <FormControl
                                style={{fontSize: 12}}
                                placeholder="Enter zip code or city"
                                value={selectedDeliveryLocation}
                                onChange={(e) => handleChangeDeliveryLocation(e.target.value)}
                            />
                            <InputGroup>
                                <Dropdown show={showDeliveryLocation}
                                          onSelect={handleSelectDeliveryLocation}>
                                    <Dropdown.Menu className="dropdown-scrollable">
                                        {filteredItemsDelivery.map((item, idx) => (
                                            <Dropdown.Item
                                                eventKey={`${item.name}, ${item.city}, ${item.state}`}
                                                key={idx}>
                                                {`${item.name}, ${item.city}, ${item.state}`}
                                            </Dropdown.Item>
                                        ))}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </InputGroup>
                        </InputGroup>
                    </Form.Group>
                    <div className="question">
                        <label>Select Transport type</label>
                        <div className="question-answer">
                            <div className="d-flex">
                                <div className="col-md-4">
                                    <input
                                        type="radio"
                                        value="open"
                                        id={`radio_1_${formId}`}
                                        name="transportType"
                                        checked={transportType === 'open'}
                                        onChange={handleChange}
                                    />
                                    <label htmlFor={`radio_1_${formId}`} className="radio">
                                        <span>Open</span>
                                    </label>
                                </div>
                                <div className="col-md-4">
                                    <input
                                        type="radio"
                                        value="enclosed"
                                        id={`radio_2_${formId}`}
                                        name="transportType"
                                        checked={transportType === 'enclosed'}
                                        onChange={handleChange}
                                    />
                                    <label htmlFor={`radio_2_${formId}`} className="radio">
                                        <span>Enclosed</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="btn-block">
                        <button type="submit">
                            SUBMIT
                        </button>
                    </div>
                </Form>
            </div>
        </>
    );
};

export default QuoteForm;