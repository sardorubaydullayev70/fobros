import React, {useEffect, useState} from 'react';
import {NavLink} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBars, faTimes, faCaretDown, faCaretUp} from '@fortawesome/free-solid-svg-icons';

const NavBar: React.FC = () => {
    const [isOffCanvas, setOffCanvas] = useState<boolean>(false);
    const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
    const [isExpanded, setExpanded] = useState<boolean>(false);

    // Update window width when the window size changes
    useEffect(() => {
        const handleResize = () => setWindowWidth(window.innerWidth);
        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    // Close off-canvas menu when window width > 768
    useEffect(() => {
        if (windowWidth > 1200 && isOffCanvas) {
            setOffCanvas(false);
        }
    }, [windowWidth, isOffCanvas]);

    const handleExpand = () => setExpanded(!isExpanded);

    const handleOffCanvasToggle = () => setOffCanvas(!isOffCanvas);

    return (
        <>
            <div className="site-mobile-menu">
                <div className="site-mobile-menu-header">
                    <div className="site-mobile-menu-close mt-3">
                        <FontAwesomeIcon
                            icon={faTimes}
                            size={'2x'}
                            className={'js-menu-toggle'}
                            onClick={handleOffCanvasToggle}
                        />
                    </div>
                </div>

                {isOffCanvas && (
                    <div className="site-mobile-menu-body">
                        <ul  className="site-nav-wrap">
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/about-us">About Us</NavLink>
                            </li>
                            <li className="has-children nav-item">
                                <NavLink className="nav-link" to="/company" onClick={handleExpand}>
                                    Company
                                    <FontAwesomeIcon
                                        icon={isExpanded ? faCaretUp : faCaretDown}
                                        className={'arrow-collapse collapsed'}
                                    />
                                </NavLink>

                                {isExpanded && (
                                    <ul className="dropdown">
                                        <li className="nav-item">
                                            <NavLink className="nav-link" to="/company#reviews">Reviews</NavLink>
                                        </li>
                                        <li className="nav-item">
                                            <NavLink className="nav-link" to="/company#vision-and-mission">
                                                Vision and Mission
                                            </NavLink>
                                        </li>
                                        <li className="nav-item">
                                            <NavLink className="nav-link" to="/company#leadership-team">
                                                Leadership Team
                                            </NavLink>
                                        </li>
                                    </ul>
                                )}
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/blog">Blog</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/contact-us">Contact</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/get-a-quote">Get a Quote</NavLink>
                            </li>
                        </ul>
                    </div>
                )}
            </div>

            <header className="site-navbar py-3 d-block" role="banner">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-11 col-xl-2">
                            <h1 className="mb-0">
                                <NavLink to="/" className="text-white h2 mb-0">
                                    <img src="/assets/images/logo.png" width="135px" alt=""/>
                                </NavLink>
                            </h1>
                        </div>

                        <div className="col-12 col-md-10 d-none d-xl-block">
                            <nav
                                className={`site-navigation position-relative text-right ${isOffCanvas ? 'offcanvas-menu' : ''}`}
                                role="navigation">
                                <ul className="site-menu js-clone-nav mx-auto d-none d-lg-block navbar-nav">
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/">Home</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/about-us">About Us</NavLink>
                                    </li>
                                    <li onMouseEnter={handleExpand}
                                        onMouseLeave={handleExpand}
                                        className="has-children nav-item">
                                        <NavLink className="nav-link" to="/company">Company
                                            <FontAwesomeIcon
                                                icon={isExpanded ? faCaretUp : faCaretDown}
                                                className={'arrow-collapse collapsed'}
                                            />
                                        </NavLink>

                                        <ul className="dropdown">
                                            <li className="nav-item">
                                                <NavLink className="nav-link" to="company#reviews">Reviews</NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink className="nav-link" to="company#vision-and-mission">Vision and
                                                    Mission</NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink className="nav-link" to="company#leadership-team">Leadership
                                                    Team</NavLink>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/blog">Blog</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to={"/contact-us"}>Contact</NavLink>
                                    </li>
                                    <li id="quote-btn">
                                        <a href="/#">Get a Quote</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div
                            className="d-inline-block d-xl-none ml-md-0 mr-auto py-3"
                            style={{position: 'relative', top: 3}}
                        >
                            <a href="#" className="site-menu-toggle js-menu-toggle text-white"
                               onClick={handleOffCanvasToggle}>
                                <FontAwesomeIcon icon={faBars} className="h3"/>
                            </a>
                        </div>
                    </div>
                </div>
            </header>
        </>
    );
};

export default NavBar;
