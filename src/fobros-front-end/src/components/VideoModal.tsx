import React, {useState} from 'react';
import {Button, Modal} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlay} from "@fortawesome/free-solid-svg-icons";

const VideoModal: React.FC = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
    }
    const handleShow = () => {
        setShow(true);
    }

    const videoSrc = "https://www.youtube.com/embed/LXb3EKWsInQ";

    return (
        <div className="bs-example">
            <center>
                <p
                    onClick={handleShow}
                    className="play-single-big mb-4 d-inline-block align-content-center"
                >
                    <FontAwesomeIcon icon={faPlay} size={'2x'} />
                    {/*<span className="icon-play"/>*/}
                </p>

                <Modal size={'lg'} show={show} onHide={handleClose}>
                    <Modal.Body>
                        <div className="embed-responsive embed-responsive-16by9">
                            <iframe
                                className="embed-responsive-item"
                                src={show ? videoSrc : undefined}
                                frameBorder="0"
                                allowFullScreen
                            ></iframe>
                        </div>
                    </Modal.Body>
                </Modal>
            </center>
        </div>
    );
}

export default VideoModal;
