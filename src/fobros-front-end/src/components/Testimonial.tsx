import React, {FunctionComponent, useEffect, useRef} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faStar} from '@fortawesome/free-solid-svg-icons';

type TestimonialProps = {
    aosDelayTime: number,
    name: string,
    rating: number
    socialNetworkLogo?: string,
    socialNetworkLink?: string,
    feedback: string,
    maxLines: number;
}
const Testimonial: FunctionComponent<TestimonialProps> = ({
                                                              aosDelayTime,
                                                              name,
                                                              rating,
                                                              socialNetworkLogo,
                                                              socialNetworkLink,
                                                              feedback,
                                                              maxLines
                                                          }) => {

    // Prepare an array to contain the stars
    const stars = [];

    // Fill in the solid stars
    for (let i = 0; i < rating; i++) {
        stars.push(<FontAwesomeIcon key={i} icon={faStar} className="rating-color"/>);
    }

    // If there are less than 5 stars, fill in the rest with regular stars
    for (let i = rating; i < 5; i++) {
        stars.push(<FontAwesomeIcon key={i} icon={faStar}/>);
    }
    const containerRef = useRef<HTMLDivElement>(null);
    useEffect(() => {
        const container = containerRef.current;
        const paragraph = container?.querySelector('p');

        const truncateText = () => {
            if (!container || !paragraph) return;

            while (paragraph.offsetHeight > container.offsetHeight) {
                const content = paragraph.textContent;
                if (!content) break;

                paragraph.textContent = content.slice(0, -1);
            }
        };

        truncateText();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    return (
        <>
            <div
                className="position-relative py-4 col-md-1 col-lg-3 mb-5 mb-lg-0  rounded border lg-mw-24"
                data-aos="fade"
                data-aos-delay={aosDelayTime}
            >
                <svg width="136.795" height="136.795" viewBox="0 0 97 97" fill="none" xmlns="http://www.w3.org/2000/svg"
                     className="pattern-right" data-v-e4caeaf8="">
                    <path d="M1.01807e-05 0.999993L88 1C92.4183 1 96 4.58172 96 9L96 97" stroke="orange"
                          strokeWidth="1.6" data-v-e4caeaf8=""></path>
                </svg>
                <svg width="136.795" height="136.795" viewBox="0 0 97 97" fill="none" xmlns="http://www.w3.org/2000/svg"
                     className="pattern-left" data-v-e4caeaf8="">
                    <path d="M97 96H9C4.58172 96 1 92.4183 1 88V0" stroke="orange" strokeWidth="1.6"
                          data-v-e4caeaf8=""></path>
                </svg>
                <div className="person">
                    <h5>{name}</h5>
                    <div className="d-flex justify-content-between">
                        <div className="ratings">{stars}</div>
                        <a href={socialNetworkLink}>
                            <img src={socialNetworkLogo} alt="social-network-logo"
                                 className="social-network-logo mb-3"/>
                        </a>
                    </div>
                    <p className="mb-4 text-justify substring-testimonial-text" ref={containerRef}>
                        {feedback}
                    </p>
                </div>
            </div>
        </>
    );
};

export default Testimonial;