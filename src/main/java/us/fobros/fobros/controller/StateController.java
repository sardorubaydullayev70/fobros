package us.fobros.fobros.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import us.fobros.fobros.entity.State;
import us.fobros.fobros.service.StateService;

@RestController
@RequestMapping("/api/v1/locations")
@CrossOrigin
public class StateController {

    private final StateService stateService;

    public StateController(StateService stateService) {
        this.stateService = stateService;
    }


    @GetMapping("/")
    public Page<State> getStates(@RequestParam(required = false) String searchText, @RequestParam(defaultValue = "10") Integer page) {
        return stateService.getState(searchText, page-1);
    }
}




