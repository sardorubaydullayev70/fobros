package us.fobros.fobros.service;

import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import us.fobros.fobros.entity.State;
import us.fobros.fobros.repo.StateRepository;

import java.util.List;

@Service
public class ZipCodeService {


    private final StateRepository stateRepository;
    private final WebClient webClient;

    public ZipCodeService(StateRepository stateRepository, WebClient.Builder webClientBuilder) {
        this.stateRepository = stateRepository;
        this.webClient = webClientBuilder.baseUrl(API_URL).build();
    }

    private static final String API_URL = "https://back.usstartruckingllc.com/api/shipping/zip-codes/?size=24";
    int count = 0;

    public void loadLocations(String searchParam) {
        String url = API_URL + searchParam;

        Mono<ApiResponse> response = this.webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(ApiResponse.class);

        response.subscribe(apiResponse -> {
            List<State> zipCodes = apiResponse.getResults();
            stateRepository.saveAll(zipCodes);

            if (apiResponse.getLinks() != null && apiResponse.getLinks().getNext() != null) {
                System.out.println(count++);
                loadLocations("&page=" + (apiResponse.getCurrent_page() + 1));
            }
        });
    }

    // inner class to map the 'links' object in the JSON response
    @Data
    public static class ApiLinks {
        private String next;
        private String previous;

    }

    // inner class to map the overall JSON response
    @Data
    public static class ApiResponse {
        private ApiLinks links;
        private int page_size;
        private int current_page;
        private int total_pages;
        private int page_items;
        private int total;
        private List<State> results;

    }
}
