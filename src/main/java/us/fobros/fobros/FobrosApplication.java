package us.fobros.fobros;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import us.fobros.fobros.service.ZipCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@SpringBootApplication
@Slf4j
public class FobrosApplication {

    @Value("${fetch-locations-from-api}")
    private boolean fetchLocations;

    public static void main(String[] args) {
        SpringApplication.run(FobrosApplication.class, args);
    }

    @Bean
    public ApplicationRunner applicationRunner(ZipCodeService zipCodeService) {
        return args -> {
            if (fetchLocations){
                log.info("Fetching locations");
                zipCodeService.loadLocations("");
                log.info("Locations fetched...");
            }
        };
    }

}
